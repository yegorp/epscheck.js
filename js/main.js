/**
 * Created by Admin on 15.08.2015.
 */
var fs = require('fs');
var path = require('path');
var readline = require('readline');

var filesCount = 0;
var epsFilesCount = 0;

var errorsHolder = document.getElementById('errors');
var fileDialog = document.getElementById('fileDialog');
var filesHolder = document.getElementById('holder');
var pairsResultHolder = document.getElementById('pairs');
var sizeResultHolder = document.getElementById('size');
var versionResultHolder = document.getElementById('version');
var summaryHolder = document.getElementById('summary');

if (fileDialog.addEventListener) {
    fileDialog.addEventListener("change", handlFunction, false);
} else if (fileDialog.attachEvent) {
    fileDialog.attachEvent("onchange", handlFunction)
}

function handlFunction(e) {
    reInit();
    var currentDirPath = this.value;
    fs.readdir(currentDirPath, function (err, items) {
        if (items) {
            for (var i = 0; i < items.length; i++) {
                processFileItem(path.join(currentDirPath, items[i]));
            }
        }
    });
    printSummary();
}

// prevent default behavior from changing page on dropped file
window.ondragover = function (e) {
    e.preventDefault();
    return false
};
window.ondrop = function (e) {
    e.preventDefault();
    return false
};

filesHolder.ondragover = function () {
    this.className = 'hover';
    return false;
};
filesHolder.ondragleave = function () {
    this.className = '';
    return false;
};
filesHolder.ondrop = function (e) {
    e.preventDefault();
    reInit();
    for (var i = 0; i < e.dataTransfer.files.length; ++i) {
        processFileItem(e.dataTransfer.files[i].path)
    }
    printSummary();
    return false;
};

function processFileItem(item) {
    fs.stat(item, function (err, stats) {
        if (stats.isFile()) {
            filesCount++;
            if (path.extname(item) === '.eps') {
                epsFilesCount++;
                checkPair(item, printResult);
                checkSize(item, stats, printResult);
                checkEpsVersion(item, printResult);
            }
        }
        printSummary();
    })
}

function reInit() {
    filesCount = 0;
    epsFilesCount = 0;
    while (errorsHolder.firstChild) errorsHolder.removeChild(errorsHolder.firstChild);
    while (pairsResultHolder.firstChild) pairsResultHolder.removeChild(pairsResultHolder.firstChild);
    while (sizeResultHolder.firstChild) sizeResultHolder.removeChild(sizeResultHolder.firstChild);
    while (versionResultHolder.firstChild) versionResultHolder.removeChild(versionResultHolder.firstChild);
    while (summaryHolder.firstChild) summaryHolder.removeChild(summaryHolder.firstChild);
}

function checkSize(filePath, stats, callback) {
    if (stats["size"] > 15360000 || stats["size"] === 0) callback("Check size: " + filePath, sizeResultHolder);
}

function checkPair(filePath, callback) {
    if (!fs.existsSync(filePath.replace(/\.eps$/, '.jpg'))) callback("Check pair: " + filePath, pairsResultHolder);
}

function checkEpsVersion(filePath, callback) {
    var versionToCheck = "%%Creator: Adobe Illustrator(R) 10.0";
    var rightVersion = false;
    var rl = readline.createInterface({
        input: fs.createReadStream(filePath, {encoding: "ASCII"}),
        terminal: false
    });
    rl.on("line", function (ln) {
        if (ln === versionToCheck) {
            rightVersion = true;
            rl.close();
        }
    });
    rl.on("close", function () {
        if (!rightVersion) {
            callback("Check version: " + filePath, versionResultHolder);
        }
    });
}

function printResult(result, outputNode) {
    var p = document.createElement("p");
    p.innerHTML = result;
    outputNode.appendChild(p);
}

function printSummary() {
    while (summaryHolder.firstChild) summaryHolder.removeChild(summaryHolder.firstChild);
    printResult('Total files processed: ' + filesCount + '; .eps files checked: ' + epsFilesCount + '.', summaryHolder);
}

process.on('uncaughtException', function (e) {
    printResult(e, errorsHolder);
});

//fs.watch('./', function () {
//    if (location) location.reload();
//});